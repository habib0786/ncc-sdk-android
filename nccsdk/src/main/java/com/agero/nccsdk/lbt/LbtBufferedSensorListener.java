package com.agero.nccsdk.lbt;

import com.agero.nccsdk.NccSensorListener;
import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.domain.data.NccSensorData;
import com.agero.nccsdk.lbt.network.LbtSyncManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by james hermida on 11/1/17.
 */

class LbtBufferedSensorListener<T extends NccSensorData> implements NccSensorListener {

    private final LbtSyncManager lbtSyncManager;
    private final int maxBufferSize;

    private final List<T> buffer = new ArrayList<>();

    LbtBufferedSensorListener(LbtSyncManager lbtSyncManager, int maxBufferSize) {
        this.maxBufferSize = maxBufferSize;
        this.lbtSyncManager = lbtSyncManager;
    }

    @Override
    public void onDataChanged(NccSensorType sensorType, NccSensorData data) {
        T concreteData = (T) data;

        if (buffer.size() == maxBufferSize) {
            lbtSyncManager.streamData(sensorType, buffer);
            buffer.clear();
        }

        buffer.add(concreteData);
    }
}
