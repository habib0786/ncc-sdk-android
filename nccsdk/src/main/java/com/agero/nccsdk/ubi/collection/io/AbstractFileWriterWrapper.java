package com.agero.nccsdk.ubi.collection.io;

import com.agero.nccsdk.internal.common.io.FileWriterWrapper;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Locale;

import static com.agero.nccsdk.ubi.collection.io.Headers.HEADER_KEY_VALUE_DELIMITER;
import static com.agero.nccsdk.ubi.collection.io.Headers.HEADER_KEY_VALUE_EQUALS_CHAR;
import static com.agero.nccsdk.ubi.collection.io.Headers.HEADER_ROW_PREFIX;

public abstract class AbstractFileWriterWrapper implements FileWriterWrapper {

    final Writer writer;
    private final StringBuilder stringBuilder;

    AbstractFileWriterWrapper(Writer writer, Headers headers) throws IOException {
        this.writer = writer;
        this.stringBuilder = new StringBuilder();
        writeHeaders(headers);
    }

    private void writeHeaders(Headers headers) throws IOException {
        // Only append headers when the file is first being created
        if (headers != null && headers.getRows().size() > 0) {
            for (List<Headers.HeaderKeyValue> row : headers.getRows()) {
                String line = HEADER_ROW_PREFIX;
                for (Headers.HeaderKeyValue keyValue : row) {
                    line += String.format(Locale.getDefault(), "%s%s%s%s", keyValue.getKey(), HEADER_KEY_VALUE_EQUALS_CHAR, keyValue.getValue(), HEADER_KEY_VALUE_DELIMITER);
                }
                line = line.substring(0, line.lastIndexOf(HEADER_KEY_VALUE_DELIMITER));
                appendLine(line);
                writer.flush(); // ensure headers are immediately written to the file
            }
        }
    }

    @Override
    public void appendLine(String s) throws IOException {
        stringBuilder.delete(0, stringBuilder.length());
        stringBuilder.append(s).append("\n");
        writer.append(stringBuilder.toString());
    }

    @Override
    public void close() throws IOException {
        writer.flush();
        writer.close();
    }
}
