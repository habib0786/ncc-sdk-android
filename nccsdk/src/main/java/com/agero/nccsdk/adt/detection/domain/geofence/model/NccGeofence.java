package com.agero.nccsdk.adt.detection.domain.geofence.model;

import com.google.android.gms.location.Geofence;

/**
 * A single Geofence object, defined by its center (latitude and longitude position) and radius.
 */
public class NccGeofence {
    private final String id;
    private final double latitude;
    private final double longitude;
    private final float radius;
    private final long expirationDuration;
    private final int transitionType;

    /**
     * @param geofenceId The Geofence's request ID
     * @param latitude   Latitude of the Geofence's center. The value is not checked for validity.
     * @param longitude  Longitude of the Geofence's center. The value is not checked for validity.
     * @param radius     Radius of the geofence circle. The value is not checked for validity
     * @param expiration Geofence expiration duration in milliseconds The value is not checked for
     *                   validity.
     * @param transition Type of Geofence transition. The value is not checked for validity.
     */
    public NccGeofence(
            String geofenceId,
            double latitude,
            double longitude,
            float radius,
            long expiration,
            int transition) {
        // An identifier for the geofence
        this.id = geofenceId;

        // Center of the geofence
        this.latitude = latitude;
        this.longitude = longitude;

        // Radius of the geofence, in meters
        this.radius = radius;

        // Expiration time in milliseconds
        this.expirationDuration = expiration;

        // Transition type
        this.transitionType = transition;
    }

    /**
     * Get the geofence ID
     *
     * @return A NccGeofence ID
     */
    private String getId() {
        return id;
    }

    /**
     * Get the geofence latitude
     *
     * @return A latitude value
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * Get the geofence longitude
     *
     * @return A longitude value
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * Get the geofence radius
     *
     * @return A radius value
     */
    public float getRadius() {
        return radius;
    }

    /**
     * Get the geofence expiration duration
     *
     * @return Expiration duration in milliseconds
     */
    public long getExpirationDuration() {
        return expirationDuration;
    }

    /**
     * Get the geofence transition type
     *
     * @return Transition type (see Geofence)
     */
    public int getTransitionType() {
        return transitionType;
    }

    /**
     * Creates a Location Services Geofence object from a
     * NccGeofence.
     *
     * @return A Geofence object
     */
    public Geofence toGeofence() {
        // Build a new Geofence object
        return new Geofence.Builder()
                .setRequestId(getId())
                .setTransitionTypes(transitionType)
                .setCircularRegion(
                        getLatitude(),
                        getLongitude(),
                        getRadius())
                .setExpirationDuration(expirationDuration)
                .build();
    }
}
