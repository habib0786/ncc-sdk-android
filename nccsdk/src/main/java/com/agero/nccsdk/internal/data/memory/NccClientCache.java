package com.agero.nccsdk.internal.data.memory;

import com.agero.nccsdk.TrackingContext;

/**
 * Created by james hermida on 2/8/18.
 */

public class NccClientCache implements ClientCache {

    private TrackingContext trackingContext;

    @Override
    public void setTrackingContext(TrackingContext trackingContext) {
        this.trackingContext = trackingContext;
    }

    @Override
    public TrackingContext getTrackingContext() {
        return trackingContext;
    }

    @Override
    public void clearTrackingContext() {
        trackingContext = null;
    }
}
