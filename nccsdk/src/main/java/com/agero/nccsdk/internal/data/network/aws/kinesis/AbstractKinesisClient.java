package com.agero.nccsdk.internal.data.network.aws.kinesis;

import com.agero.nccsdk.internal.auth.AuthManager;
import com.agero.nccsdk.internal.log.Timber;
import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.mobileconnectors.kinesis.kinesisrecorder.AbstractKinesisRecorder;
import com.amazonaws.mobileconnectors.kinesis.kinesisrecorder.KinesisRecorder;
import com.amazonaws.regions.Regions;

import java.io.File;

import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by james hermida on 11/13/17.
 */

public abstract class AbstractKinesisClient implements KinesisClient {

    private final AbstractKinesisRecorder recorder;
    private final AuthManager authManager;

    AbstractKinesisClient(File directory, AWSCredentialsProvider credentialsProvider, AuthManager authManager) {
        this.recorder = new KinesisRecorder(directory, Regions.US_EAST_1, credentialsProvider);
        this.authManager = authManager;
    }

    @Override
    public void save(String data, String streamName) {
        recorder.saveRecord(data, streamName);
    }

    @Override
    public void submit() {
        authManager.hasAuthenticated()
                .subscribeOn(Schedulers.io())
                .subscribe(new DisposableSingleObserver<Boolean>() {
                    @Override
                    public void onSuccess(Boolean hasAuthenticated) {
                        if (hasAuthenticated) {
                            try {
                                recorder.submitAllRecords();
                            } catch (AmazonClientException ace) {
                                Timber.e(ace, "Exception submitting kinesis records. Bytes used: %d", getDiskBytesUsed());
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e);
                    }
                });
    }

    @Override
    public long getDiskBytesUsed() {
        return recorder.getDiskBytesUsed();
    }
}
