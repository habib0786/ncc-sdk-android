package com.agero.nccsdk.adt;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.lang.reflect.Field;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;

/**
 * Created by james hermida on 9/14/17.
 */

@RunWith(MockitoJUnitRunner.class)
public class AdtStateMachineTest {

    private AdtStateMachine adtStateMachine;

    @Mock
    private AdtState mockAdtState;

    @Before
    public void setUp() throws Exception {
        adtStateMachine = new AdtStateMachine(mockAdtState);
    }

    @Test
    public void initWithState() {
        assertTrue(getState(adtStateMachine) instanceof AdtState);
    }

    @Test
    public void handleTrigger_start_detection() throws Exception {
        adtStateMachine.onTriggerReceived(AdtTriggerType.START_MONITORING);
        verify(mockAdtState).startMonitoring(adtStateMachine);
    }

    @Test
    public void handleTrigger_start_server() throws Exception {
        adtStateMachine.onTriggerReceived(AdtTriggerType.START_SERVER);
        verify(mockAdtState).startDriving(adtStateMachine);
    }

    @Test
    public void handleTrigger_start_motion() throws Exception {
        adtStateMachine.onTriggerReceived(AdtTriggerType.START_MOTION);
        verify(mockAdtState).startDriving(adtStateMachine);
    }

    @Test
    public void handleTrigger_start_wifi() throws Exception {
        adtStateMachine.onTriggerReceived(AdtTriggerType.START_WIFI);
        verify(mockAdtState).startDriving(adtStateMachine);
    }

    @Test
    public void handleTrigger_start_geofence() throws Exception {
        adtStateMachine.onTriggerReceived(AdtTriggerType.START_GEOFENCE);
        verify(mockAdtState).startDriving(adtStateMachine);
    }

    @Test
    public void handleTrigger_stop_motion() throws Exception {
        adtStateMachine.onTriggerReceived(AdtTriggerType.STOP_MOTION);
        verify(mockAdtState).stopDriving(adtStateMachine);
    }

    @Test
    public void handleTrigger_stop_location_timer() throws Exception {
        adtStateMachine.onTriggerReceived(AdtTriggerType.STOP_LOCATION_TIMER);
        verify(mockAdtState).stopDriving(adtStateMachine);
    }

    @Test
    public void handleTrigger_stop_location_mode() throws Exception {
        adtStateMachine.onTriggerReceived(AdtTriggerType.STOP_LOCATION_MODE);
        verify(mockAdtState).stopDriving(adtStateMachine);
    }

    @Test
    public void handleTrigger_stop_server() throws Exception {
        adtStateMachine.onTriggerReceived(AdtTriggerType.STOP_SERVER);
        verify(mockAdtState).stopDriving(adtStateMachine);
    }

    private Object getState(AdtStateMachine adtStateMachine) {
        try {
            Field f = getField(AdtStateMachine.class, "currentState");
            f.setAccessible(true);
            f.get(adtStateMachine);

            return f.get(adtStateMachine);
        } catch (NoSuchFieldException nsfe) {
            nsfe.printStackTrace();
        } catch (IllegalAccessException iae) {
            iae.printStackTrace();
        }

        return null;
    }

    private Field getField(Class clazz, String fieldName) throws NoSuchFieldException {
        try {
            return clazz.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            Class superClass = clazz.getSuperclass();
            if (superClass == null) {
                throw e;
            } else {
                return getField(superClass, fieldName);
            }
        }
    }
}
