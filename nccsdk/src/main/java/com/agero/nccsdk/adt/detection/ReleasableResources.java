package com.agero.nccsdk.adt.detection;

public interface ReleasableResources {

    void releaseResources();

}
