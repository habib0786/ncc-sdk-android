package com.agero.nccsdk.adt.config;

import com.agero.nccsdk.internal.common.statemachine.SdkConfig;

/**
 * Created by james hermida on 11/14/17.
 */

public class SessionConfig implements SdkConfig {

    private long timeout = 1000 * 60 * 5; // default 5 minutes

    private float locationSpeedThreshold = 5.0f; // 5 m/s

    private int motionConfidenceThreshold = 50; // scale 0-100%

    private int wifiReconnectTimeThreshold = 1000 * 30; // default 30 seconds

    private int wifiDisconnectStartSessionDelay = 1000 * 30; // default 30 seconds

    public long getTimeout() {
        return timeout;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

    public float getLocationSpeedThreshold() {
        return locationSpeedThreshold;
    }

    public void setLocationSpeedThreshold(float locationSpeedThreshold) {
        this.locationSpeedThreshold = locationSpeedThreshold;
    }

    public int getMotionConfidenceThreshold() {
        return motionConfidenceThreshold;
    }

    public void setMotionConfidenceThreshold(int motionConfidenceThreshold) {
        this.motionConfidenceThreshold = motionConfidenceThreshold;
    }

    public int getWifiReconnectTimeThreshold() {
        return wifiReconnectTimeThreshold;
    }

    public void setWifiReconnectTimeThreshold(int wifiReconnectTimeThreshold) {
        this.wifiReconnectTimeThreshold = wifiReconnectTimeThreshold;
    }

    public int getWifiDisconnectStartSessionDelay() {
        return wifiDisconnectStartSessionDelay;
    }

    public void setWifiDisconnectStartSessionDelay(int wifiDisconnectStartSessionDelay) {
        this.wifiDisconnectStartSessionDelay = wifiDisconnectStartSessionDelay;
    }
}
