package com.agero.nccsdk.internal.data;

import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.TrackingContext;
import com.agero.nccsdk.adt.detection.start.model.WifiActivity;
import com.agero.nccsdk.domain.config.NccAccelerometerConfig;
import com.agero.nccsdk.internal.common.statemachine.SdkConfig;
import com.agero.nccsdk.internal.common.statemachine.SdkConfigType;
import com.agero.nccsdk.internal.data.memory.ClientCache;
import com.agero.nccsdk.internal.data.preferences.ConfigManager;
import com.agero.nccsdk.internal.data.preferences.NccSharedPrefs;
import com.agero.nccsdk.internal.log.config.LogConfig;
import com.agero.nccsdk.internal.notification.model.NccNotification;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.Constructor;
import java.security.InvalidParameterException;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

/**
 * Created by james hermida on 10/19/17.
 */

@RunWith(PowerMockRunner.class)
public class NccDataManagerTest {

    private NccDataManager nccDataManager;

    @Mock
    private ConfigManager mockConfigManager;

    @Mock
    private NccSharedPrefs mockSharedPrefs;

    @Mock
    private ClientCache mockClientCache;

    @Before
    public void setUp() throws Exception {
        Constructor[] ctors = NccDataManager.class.getDeclaredConstructors();
        nccDataManager = (NccDataManager) ctors[0].newInstance(mockConfigManager, mockSharedPrefs, mockClientCache);
    }

    @Test
    public void saveConfig_valid_config() throws Exception {
        NccSensorType testSensorType = NccSensorType.ACCELEROMETER;
        NccAccelerometerConfig testConfig = new NccAccelerometerConfig();

        nccDataManager.saveConfig(testSensorType, testConfig);
        verify(mockConfigManager).saveConfig(testSensorType, testConfig);
    }

    @Test
    public void saveConfig_null_config() throws Exception {
        NccSensorType testSensorType = NccSensorType.ACCELEROMETER;
        NccAccelerometerConfig nullConfig = null;
        try {
            nccDataManager.saveConfig(testSensorType, nullConfig);
        } catch (Exception e) {
            assertTrue(e instanceof java.security.InvalidParameterException);
        }
    }

    @Test
    public void getConfig() throws Exception {
        NccSensorType testSensorType = NccSensorType.ACCELEROMETER;

        nccDataManager.getConfig(testSensorType);
        verify(mockConfigManager).getConfig(testSensorType);
    }

    @Test
    public void saveSdkConfig() throws Exception {
        SdkConfigType configType = SdkConfigType.LOG;
        SdkConfig config = new LogConfig();
        nccDataManager.saveSdkConfig(configType, config);
        verify(mockConfigManager).saveSdkConfig(configType, config);
    }

    @Test
    public void saveSdkConfig_null_config() throws Exception {
        try {
            nccDataManager.saveSdkConfig(SdkConfigType.LOG, null);
        } catch (Exception e) {
            assertTrue(e instanceof InvalidParameterException);
            verifyZeroInteractions(mockConfigManager);
        }
    }

    @Test
    public void getSdkConfig() throws Exception {
        SdkConfigType configType = SdkConfigType.LOG;
        nccDataManager.getSdkConfig(configType);
        verify(mockConfigManager).getSdkConfig(configType);
    }

    @Test
    public void setUserId_valid_string() throws Exception {
        String testUserId = "testUserId";

        nccDataManager.setUserId(testUserId);
        verify(mockSharedPrefs).setUserId(testUserId);
    }

    @Test
    public void setUserId_empty_id() throws Exception {
        String testUserId = "";

        try {
            nccDataManager.setUserId(testUserId);
        } catch (Exception e) {
            assertTrue(e instanceof java.security.InvalidParameterException);
        }
    }

    @Test
    public void setUserId_null_id() throws Exception {
        String testUserId = null;

        try {
            nccDataManager.setUserId(testUserId);
        } catch (Exception e) {
            assertTrue(e instanceof java.security.InvalidParameterException);
        }
    }

    @Test
    public void getUserId() throws Exception {
        nccDataManager.getUserId();
        verify(mockSharedPrefs).getUserId();
    }

    @Test
    public void setTrackingContext() throws Exception {
        TrackingContext trackingContext = new TrackingContext();
        trackingContext.putString("test", "test");
        nccDataManager.setTrackingContext(trackingContext);
        verify(mockClientCache).setTrackingContext(trackingContext);
    }

    @Test
    public void setTrackingContext_null_context() throws Exception {
        try {
            nccDataManager.setTrackingContext(null);
        } catch (Exception e) {
            assertTrue(e instanceof InvalidParameterException);
            verifyZeroInteractions(mockClientCache);
        }
    }

    @Test
    public void getTrackingContext() throws Exception {
        TrackingContext trackingContext = new TrackingContext();
        trackingContext.putString("test", "test");
        nccDataManager.setTrackingContext(trackingContext);

        nccDataManager.getTrackingContext();
        verify(mockClientCache).getTrackingContext();
    }

    @Test
    public void clearTrackingContext() throws Exception {
        TrackingContext trackingContext = new TrackingContext();
        trackingContext.putString("test", "test");
        nccDataManager.setTrackingContext(trackingContext);

        nccDataManager.clearTrackingContext();
        verify(mockClientCache).clearTrackingContext();
        assertNull(nccDataManager.getTrackingContext());
    }

    @Test
    public void setRecentWifiActivity_null_activity() throws Exception {
        try {
            nccDataManager.setRecentWifiActivity(null);
        } catch (Exception e) {
            assertTrue(e instanceof java.security.InvalidParameterException);
            verifyZeroInteractions(mockSharedPrefs);
        }
    }

    @Test
    public void setRecentWifiActivity() throws Exception {
        WifiActivity mockWifiActivity = mock(WifiActivity.class);
        nccDataManager.setRecentWifiActivity(mockWifiActivity);
        verify(mockSharedPrefs).setRecentWifiActivity(mockWifiActivity);
    }

    @Test
    public void getRecentWifiActivity() throws Exception {
        nccDataManager.getRecentWifiActivity();
        verify(mockSharedPrefs).getRecentWifiActivity();
    }

    @Test
    public void saveNotification_null_notification() throws Exception {
        try {
            nccDataManager.saveNotification(null);
        } catch (Exception e) {
            assertTrue(e instanceof java.security.InvalidParameterException);
            verifyZeroInteractions(mockSharedPrefs);
        }
    }

    @Test
    public void saveNotification() throws Exception {
        NccNotification notification = mock(NccNotification.class);
        nccDataManager.saveNotification(notification);
        verify(mockSharedPrefs).saveNotification(notification);
    }

    @Test
    public void getNotification() throws Exception {
        String id = "id";
        nccDataManager.getNotification(id);
        verify(mockSharedPrefs).getNotification(id);
    }

    @Test
    public void setLbtAutoRestart() throws Exception {
        boolean bool = true;
        nccDataManager.setLbtAutoRestart(bool);
        verify(mockSharedPrefs).setLbtAutoRestart(bool);
    }

    @Test
    public void shouldLbtAutoRestart() throws Exception {
        nccDataManager.shouldLbtAutoRestart();
        verify(mockSharedPrefs).shouldLbtAutoRestart();
    }

    @Test
    public void setAuthenticatedApiKey_empty_null_key() throws Exception {
        try {
            nccDataManager.setAuthenticatedApiKey("");
        } catch (Exception e) {
            assertTrue(e instanceof java.security.InvalidParameterException);
            verifyZeroInteractions(mockSharedPrefs);
        }

        try {
            nccDataManager.setAuthenticatedApiKey(null);
        } catch (Exception e) {
            assertTrue(e instanceof java.security.InvalidParameterException);
            verifyZeroInteractions(mockSharedPrefs);
        }
    }

    @Test
    public void setAuthenticatedApiKey() throws Exception {
        String testApiKey = "testApiKey";
        nccDataManager.setAuthenticatedApiKey(testApiKey);
        verify(mockSharedPrefs).setAuthenticatedApiKey(testApiKey);
    }

    @Test
    public void getAuthenticatedApiKey() throws Exception {
        nccDataManager.getAuthenticatedApiKey();
        verify(mockSharedPrefs).getAuthenticatedApiKey();
    }

    @Test
    public void setAccessId_empty_null_id() throws Exception {
        try {
            nccDataManager.setAccessId("");
        } catch (Exception e) {
            assertTrue(e instanceof java.security.InvalidParameterException);
            verifyZeroInteractions(mockSharedPrefs);
        }

        try {
            nccDataManager.setAccessId(null);
        } catch (Exception e) {
            assertTrue(e instanceof java.security.InvalidParameterException);
            verifyZeroInteractions(mockSharedPrefs);
        }
    }

    @Test
    public void setAccessId() throws Exception {
        String testAccessId = "testAccessId";
        nccDataManager.setAccessId(testAccessId);
        verify(mockSharedPrefs).setAccessId(testAccessId);
    }

    @Test
    public void getAccessId() throws Exception {
        nccDataManager.getAccessId();
        verify(mockSharedPrefs).getAccessId();
    }
}
