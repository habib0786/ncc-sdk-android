package com.agero.nccsdk.lbt.util;

/**
 * Created by james hermida on 11/8/17.
 *
 * Interface for an operation to be executed on a time interval
 */

public interface TimedOperation {

    void start(long timeInterval);
    void stop();
    boolean isRunning();

}
