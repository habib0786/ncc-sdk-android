package com.agero.nccsdk.internal.data.network.aws.s3;

import com.agero.nccsdk.internal.auth.AuthManager;
import com.agero.nccsdk.internal.common.util.FileUtils;
import com.agero.nccsdk.internal.common.util.StringUtils;
import com.agero.nccsdk.internal.data.network.aws.util.TransferUtils;
import com.agero.nccsdk.internal.log.Timber;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferType;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;

import java.io.File;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

@Singleton
public class S3TransferManager implements FileTransferManager {

    private final AuthManager authManager;
    private final TransferUtility s3TransferUtility;
    private final TransferListener uploadTransferListener = new TransferListener() {
        @Override
        public void onStateChanged(int id, TransferState state) {
            Timber.v("%s - Transfer state changed: %s", id, TransferUtils.getTransferStateReadableString(state));

            // Delete uploaded file
            if (state == TransferState.COMPLETED) {
                File completedFile = new File(s3TransferUtility.getTransferById(id).getAbsoluteFilePath());
                if (completedFile.delete()) {
                    Timber.d("Uploaded file deleted: %s", completedFile);
                } else {
                    Timber.w("Failed to delete uploaded file: %s", completedFile);
                }
            }
        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

        }

        @Override
        public void onError(int id, Exception ex) {
            File file = new File(s3TransferUtility.getTransferById(id).getAbsoluteFilePath());
            Timber.e(ex, "Failed upload transfer id %s: %s", id, file);
        }
    };

    @Inject
    S3TransferManager(TransferUtility s3TransferUtility, AuthManager authManager) {
        this.s3TransferUtility = s3TransferUtility;
        this.authManager = authManager;
    }

    @Override
    public void uploadFile(String targetDestination, File file) {
        if (StringUtils.isNullOrEmpty(targetDestination)) {
            Timber.e("uploadFile() - target destination is null or empty");
        } else if (FileUtils.isNullOrDoesNotExist(file)) {
            Timber.e("uploadFile() - File is null or does not exist");
        } else if (hasFileAlreadyBeenQueuedForUpload(file)) {
            Timber.w("File has already been queued for upload: %s", file.getAbsolutePath());
        } else {
            authManager.hasAuthenticated()
                    .subscribeOn(Schedulers.io())
                    .subscribe(new DisposableSingleObserver<Boolean>() {
                        @Override
                        public void onSuccess(Boolean hasAuthenticated) {
                            if (hasAuthenticated) {
                                TransferObserver observer = s3TransferUtility.upload(
                                        targetDestination, // bucket
                                        file.getName(), // object key
                                        file
                                );

                                observer.setTransferListener(uploadTransferListener);
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Timber.e(e);
                        }
                    });
        }
    }

    private boolean hasFileAlreadyBeenQueuedForUpload(File file) {
        if (FileUtils.isNull(file)) {
            Timber.w("hasFileAlreadyBeenQueuedForUpload() - file is null");
        } else {
            List<TransferObserver> observers = s3TransferUtility.getTransfersWithType(TransferType.UPLOAD);

            for (int x = 0; x < observers.size(); x++) {
                TransferObserver transferObserver = observers.get(x);
                if (transferObserver.getAbsoluteFilePath().endsWith(file.getName())) {
                    if (TransferState.WAITING.equals(transferObserver.getState()) || TransferState.IN_PROGRESS.equals(transferObserver.getState()) || TransferState.WAITING_FOR_NETWORK.equals(transferObserver.getState())) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}
