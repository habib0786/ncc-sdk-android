package com.agero.nccsdk;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

import com.agero.nccsdk.internal.log.Timber;

/**
 * Class used to manage the permissions required by the SDK.
 */
public class NccPermissionsManager {

    private static final int GROUP_LOCATION = 0;
    private static final int GROUP_PHONE = 1;

    private static boolean isPermissionGroupAllowed(int permissionGroup) {
        switch (permissionGroup) {
            case GROUP_LOCATION:
                if (!isPermissionAllowed(PERMISSIONS_LOCATION[0]))
                    return false;
                break;
            case GROUP_PHONE:
                if (!isPermissionAllowed(PERMISSIONS_PHONE[0]))
                    return false;
                break;
        }
        return false;
    }

    private static final String[] PERMISSIONS_LOCATION = new String[] {
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
    };
    private static final String[] PERMISSIONS_PHONE = new String[] {
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.PROCESS_OUTGOING_CALLS,
    };

    /**
     * Determines if a particular {@link Manifest.permission} has been allowed.
     *
     * @param permission A permission defined by {@link Manifest.permission}
     * @return true if the permission has been allowed, otherwise false
     */
    public static boolean isPermissionAllowed(String permission) {
        return ContextCompat.checkSelfPermission(NccSdk.getApplicationContext(), permission) == PackageManager.PERMISSION_GRANTED;
    }

    private static boolean isAnyPermissionDenied() {
        // Device running below Marshmallow allows all permissions
        // during installation.
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return false;
        }

        int[] permissionStates = getRequiredPermissionsStates();
        for (int i = 0; i < permissionStates.length; i++) {
            if (permissionStates[i] == PackageManager.PERMISSION_DENIED) {
                return true;
            }
        }
        return false;
    }

    private static int[] getRequiredPermissionsStates() {
        int[] result = new int[]{
                PackageManager.PERMISSION_DENIED,
                PackageManager.PERMISSION_DENIED,
                PackageManager.PERMISSION_DENIED}; // default to denied

        for (int i = 0; i < PERMISSIONS_LOCATION.length; i++) {
            if (isPermissionAllowed(PERMISSIONS_LOCATION[i])) {
                result[GROUP_LOCATION] = PackageManager.PERMISSION_GRANTED;
                // Since Android gives permission to a group if one permission
                // from that group is allowed, there's no need to go through the
                // rest of the permissions for the group in question
                break;
            }
        }

        for (int i = 0; i < PERMISSIONS_PHONE.length; i++) {
            if (isPermissionAllowed(PERMISSIONS_PHONE[i])) {
                result[GROUP_PHONE] = PackageManager.PERMISSION_GRANTED;
                // Since Android gives permission to a group if one permission
                // from that group is allowed, there's no need to go through the
                // rest of the permissions for the group in question
                break;
            }
        }

        // Log permission states
        for (int i = 0; i < result.length; i++) {
            Timber.v("%s: %s", getPermissionGroupString(i), (result[i] == PackageManager.PERMISSION_GRANTED ? "Granted" : "Denied"));
        }

        return result;
    }

    private static String getPermissionGroupString(int permissionGroup) {
        switch (permissionGroup) {
            case GROUP_LOCATION:
                return Manifest.permission_group.LOCATION;
            case GROUP_PHONE:
                return Manifest.permission_group.PHONE;
        }
        return "UNKNOWN GROUP";
    }

    /**
     * Shows a multiple permissions dialog with the permissions required by the SDK
     *
     * @param activity The activity where the permission dialog will be displayed
     */
    public static void showMultiplePermissionsDialogIfNeeded(Activity activity) {
//        if (!Validate.sdkInitialized()) {
//            return;
//        }
//
//        Validate.notNull(activity, "activity");

        List<String> permissionsList = new ArrayList<>();
        if (!isPermissionAllowed(PERMISSIONS_LOCATION[0])) {
            permissionsList.add(PERMISSIONS_LOCATION[0]);
        }

        if (!isPermissionAllowed(PERMISSIONS_LOCATION[1])) {
            permissionsList.add(PERMISSIONS_LOCATION[1]);
        }

        if (!isPermissionAllowed(PERMISSIONS_PHONE[0])) {
            permissionsList.add(PERMISSIONS_PHONE[0]);
        }

        // Request
        if (!permissionsList.isEmpty()) {
            String[] params = permissionsList.toArray(new String[permissionsList.size()]);
            ActivityCompat.requestPermissions(activity, params, 0);
        }
    }
}
