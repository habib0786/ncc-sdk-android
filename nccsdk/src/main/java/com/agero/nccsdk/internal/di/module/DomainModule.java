package com.agero.nccsdk.internal.di.module;

import android.content.Context;

import com.agero.nccsdk.domain.NccSensorManager;
import com.agero.nccsdk.domain.SensorManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by james hermida on 2/26/18.
 */

@Module
public class DomainModule {

    @Provides
    @Singleton
    SensorManager provideSensorManager(Context context) {
        return new NccSensorManager(context);
    }

}
