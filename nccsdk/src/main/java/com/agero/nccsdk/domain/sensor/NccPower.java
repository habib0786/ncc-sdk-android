package com.agero.nccsdk.domain.sensor;

import android.content.Context;
import android.content.Intent;

import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.domain.config.NccConfig;
import com.agero.nccsdk.domain.data.NccPowerData;
import com.agero.nccsdk.internal.common.util.StringUtils;
import com.agero.nccsdk.internal.log.Timber;

public class NccPower extends NccAbstractBroadcastReceiverSensor {

    public NccPower(Context context, NccConfig config) {
        super(context, NccPower.class.getSimpleName(), NccSensorType.POWER, config);
    }

    @Override
    String[] getActions() {
        return new String[]{
            Intent.ACTION_POWER_CONNECTED,
            Intent.ACTION_POWER_DISCONNECTED
        };
    }

    @Override
    boolean isLocalReceiver() {
        return false;
    }

    @Override
    public void onDataReceived(Context context, Intent intent) {
        String action = intent.getAction();

        if (StringUtils.isNullOrEmpty(action)) {
            Timber.e("Power broadcast action is null or empty");
        } else {
            boolean isConnected = false;
            if (action.equals(Intent.ACTION_POWER_CONNECTED)) {
                isConnected = true;
            } else if (action.equals(Intent.ACTION_POWER_DISCONNECTED)) {
                isConnected = false;
            }

            NccPowerData currentData = new NccPowerData(System.currentTimeMillis(), isConnected);

            postData(currentData);
        }
    }
}
