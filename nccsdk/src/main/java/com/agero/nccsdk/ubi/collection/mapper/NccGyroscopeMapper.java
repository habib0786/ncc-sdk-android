package com.agero.nccsdk.ubi.collection.mapper;

import com.agero.nccsdk.domain.data.NccGyroscopeData;
import com.agero.nccsdk.domain.data.NccSensorData;

/**
 * Created by james hermida on 10/27/17.
 */

public class NccGyroscopeMapper extends AbstractSensorDataMapper {

    @Override
    public String map(NccSensorData data) {
        NccGyroscopeData gd = (NccGyroscopeData) data;
        clearStringBuilder();
        sb.append("R,");                                // Code
        sb.append(gd.getRotationX()); sb.append(",");   // Rx
        sb.append(gd.getRotationY()); sb.append(",");   // Ry
        sb.append(gd.getRotationZ()); sb.append(",");   // Rz
        sb.append("NA,NA,NA,NA,NA,NA,");
        sb.append(gd.getTimestamp()); sb.append(",");   // UTCTime
        sb.append(getReadableTimestamp(gd.getTimestamp())); // Timestamp
        return sb.toString();
    }
}
