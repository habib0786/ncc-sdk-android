package com.agero.nccsdk.ubi.collection.mapper;

import com.agero.nccsdk.domain.data.NccGyroscopeUncalibratedData;
import com.agero.nccsdk.domain.data.NccSensorData;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertEquals;

/**
 * Created by james hermida on 12/5/17.
 */

@RunWith(MockitoJUnitRunner.class)
public class NccGyroscopeUncalibratedMapperTest {

    private AbstractSensorDataMapper mapper = new NccGyroscopeUncalibratedMapper();

    @Test
    public void map() throws Exception {
        long timeUtc = 1315314000000L;
        String formattedDate = mapper.getReadableTimestamp(timeUtc);
        float rX = 1.23f;
        float rY = 4.56f;
        float rZ = 7.89f;
        float dX = 10.1112f;
        float dY = 13.1415f;
        float dZ = 16.1718f;
        NccSensorData data = new NccGyroscopeUncalibratedData(timeUtc, rX, rY, rZ, dX, dY, dZ);

        String expected = "RR," + rX + "," + rY + "," + rZ + "," + dX + "," + dY + "," + dZ + ",NA,NA,NA," + timeUtc + "," + formattedDate;
        String actual = mapper.map(data);

        assertEquals(expected, actual);
    }
}
