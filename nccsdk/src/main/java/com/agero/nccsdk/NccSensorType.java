package com.agero.nccsdk;

/**
 * Represents the types of sensors collected by the SDK.
 */

public enum NccSensorType {

    // Google Play Services sensors
    LOCATION("Location", "G"),

    MOTION_ACTIVITY("MotionActivity", "Z"),

    GEOFENCE("STUB", "STUB"),

    // Native sensors
    ACCELEROMETER("Accelerometer", "RA"),

    LINEAR_ACCELEROMETER("LinearAccelerometer", "A"),

    GYROSCOPE("Gyroscope", "R"),

    GYROSCOPE_UNCALIBRATED("GyroscopeUncalibrated", "RR"),

    MAGNETIC_FIELD("MagneticField", "M"),

    MAGNETIC_FIELD_UNCALIBRATED("MagneticFieldUncalibrated", "RM"),

    ROTATION_VECTOR("RotationVector", "Q"),

    BAROMETER("Barometer", "P"),

    // Other
    BATTERY("Battery", "B"),

    LOCATION_MODE("LocationMode", "LM"),

    POWER("Power", "P"), // FIXME temporary short identifier

    BLUETOOTH("Bluetooth", "BT"), // FIXME temporary short identifier

    WIFI("Wifi", "W"), // FIXME temporary short identifier

    PHONE_CALLS("PhoneCalls", "PC"); // FIXME temporary short identifier

    private final String name;

    private final String shortIdentifier;

    NccSensorType(String name, String shortIdentifier) {
        this.name = name;
        this.shortIdentifier = shortIdentifier;
    }

    public String getName() {
        return name;
    }

    public String getShortIdentifier() {
        return shortIdentifier;
    }
}
