package com.agero.nccsdk.domain;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.SparseArray;

import com.agero.nccsdk.NccException;
import com.agero.nccsdk.NccExceptionErrorCode;
import com.agero.nccsdk.NccSensorListener;
import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.domain.config.NccAccelerometerConfig;
import com.agero.nccsdk.domain.config.NccBarometerConfig;
import com.agero.nccsdk.domain.config.NccBatteryConfig;
import com.agero.nccsdk.domain.config.NccBluetoothConfig;
import com.agero.nccsdk.domain.config.NccConfig;
import com.agero.nccsdk.domain.config.NccGyroscopeConfig;
import com.agero.nccsdk.domain.config.NccGyroscopeUncalibratedConfig;
import com.agero.nccsdk.domain.config.NccLinearAccelerometerConfig;
import com.agero.nccsdk.domain.config.NccLocationConfig;
import com.agero.nccsdk.domain.config.NccLocationModeConfig;
import com.agero.nccsdk.domain.config.NccMagneticFieldConfig;
import com.agero.nccsdk.domain.config.NccMagneticFieldUncalibratedConfig;
import com.agero.nccsdk.domain.config.NccMotionConfig;
import com.agero.nccsdk.domain.config.NccPhoneCallsConfig;
import com.agero.nccsdk.domain.config.NccPowerConfig;
import com.agero.nccsdk.domain.config.NccRotationVectorConfig;
import com.agero.nccsdk.domain.config.NccWifiConfig;
import com.agero.nccsdk.domain.sensor.NccAbstractSensor;
import com.agero.nccsdk.domain.sensor.NccAccelerometer;
import com.agero.nccsdk.domain.sensor.NccBarometer;
import com.agero.nccsdk.domain.sensor.NccBattery;
import com.agero.nccsdk.domain.sensor.NccBluetooth;
import com.agero.nccsdk.domain.sensor.NccGyroscope;
import com.agero.nccsdk.domain.sensor.NccGyroscopeUncalibrated;
import com.agero.nccsdk.domain.sensor.NccLinearAccelerometer;
import com.agero.nccsdk.domain.sensor.NccLocation;
import com.agero.nccsdk.domain.sensor.NccLocationMode;
import com.agero.nccsdk.domain.sensor.NccMagneticField;
import com.agero.nccsdk.domain.sensor.NccMagneticFieldUncalibrated;
import com.agero.nccsdk.domain.sensor.NccMotion;
import com.agero.nccsdk.domain.sensor.NccPhoneCalls;
import com.agero.nccsdk.domain.sensor.NccPower;
import com.agero.nccsdk.domain.sensor.NccRotationVector;
import com.agero.nccsdk.domain.sensor.NccWifi;
import com.agero.nccsdk.internal.log.Timber;

import javax.inject.Singleton;

import static com.agero.nccsdk.internal.common.util.DeviceUtils.isGooglePlayServicesAvailable;

/**
 * Created by james hermida on 8/21/17.
 *
 * Class used to maintain all sensors that are currently streaming.
 */

@Singleton
public class NccSensorManager implements SensorManager {

    private final String TAG = NccSensorManager.class.getSimpleName();

    private final Context applicationContext;

    private final SparseArray<NccAbstractSensor> sensors;

    public NccSensorManager(Context context) {
        this.applicationContext = context;
        this.sensors = new SparseArray<>();
    }

    private boolean isSensorRegistered(NccSensorType sensorType) {
        return sensors.get(sensorType.ordinal()) != null;
    }

    @Override
    public void subscribeSensorListener(NccSensorType sensorType, NccSensorListener sensorListener, NccConfig config) throws NccException {
        if (sensorListener == null) {
            throw new NccException(TAG, "sensorListener must not be null", NccExceptionErrorCode.INVALID_PARAMETER);
        } else if (config == null) {
            throw new NccException(TAG, "config must not be null", NccExceptionErrorCode.INVALID_PARAMETER);
        } else if (!isConfigMatchesType(sensorType, config)) {
            throw new NccException(TAG, "config does not match sensor type", NccExceptionErrorCode.INVALID_PARAMETER);
        } else if (!isSensorAvailable(sensorType)) {
            throw new NccException(TAG, "Sensor is unavailable on this device " + sensorType.getName(), NccExceptionErrorCode.SENSOR_UNAVAILABLE);
        } else {
            if (!isSensorRegistered(sensorType)) {
                NccAbstractSensor sensor = constructSensor(sensorType, config);
                sensors.put(sensorType.ordinal(), sensor);
            }

            NccAbstractSensor sensor = getSensor(sensorType);
            sensor.subscribe(sensorListener);
            Timber.v("Subscribed sensor: %s", sensorType.getName());

            if (!sensor.isStreaming()) {
                sensor.startStreaming();
            }
        }
    }

    @Override
    public void unsubscribeSensorListener(NccSensorType sensorType, NccSensorListener sensorListener) throws NccException {
        if (sensorListener == null) {
            throw new NccException(TAG, "sensorListener must not be null", NccExceptionErrorCode.INVALID_PARAMETER);
        } else if (!isSensorRegistered(sensorType)) {
            throw new NccException(TAG, "Cannot unsubscribe a sensor that has not previously been subscribed", NccExceptionErrorCode.UNKNOWN_ERROR);
        } else {
            NccAbstractSensor sensor = getSensor(sensorType);
            sensor.unsubscribe(sensorListener);
            Timber.v("Unsubscribed sensor: %s", sensorType.getName());

            if (!sensor.hasSubscribers()) {
                sensors.remove(sensorType.ordinal());
                sensor.stopStreaming();
            }
        }
    }

    @Override
    public void setConfig(NccSensorType sensorType, NccConfig config) throws NccException {
        if (config == null) {
            throw new NccException(TAG, "config must not be null", NccExceptionErrorCode.INVALID_PARAMETER);
        } else if (!isConfigMatchesType(sensorType, config)) {
            throw new NccException(TAG, "config does not match sensor type", NccExceptionErrorCode.INVALID_PARAMETER);
        } else if (!isSensorRegistered(sensorType)) {
            throw new NccException(TAG, "Sensor must be subscribed before setting its configuration", NccExceptionErrorCode.UNKNOWN_ERROR);
        } else {
            NccAbstractSensor sensor = getSensor(sensorType);
            sensor.setConfig(config);
        }
    }

    @Override
    public NccConfig getConfig(NccSensorType sensorType) throws NccException {
        if (!isSensorRegistered(sensorType)) {
            throw new NccException(TAG, "Sensor must be subscribed before getting its configuration", NccExceptionErrorCode.UNKNOWN_ERROR);
        } else {
            NccAbstractSensor sensor = getSensor(sensorType);
            return sensor.getConfig();
        }
    }

    private boolean isConfigMatchesType(NccSensorType sensorType, NccConfig config) throws NccException {
        switch (sensorType) {
            // Google Play Services sensors
            case LOCATION:
                return config instanceof NccLocationConfig;
            case MOTION_ACTIVITY:
                return config instanceof NccMotionConfig;

            // Native sensors
            case ACCELEROMETER:
                return config instanceof NccAccelerometerConfig;
            case LINEAR_ACCELEROMETER:
                return config instanceof NccLinearAccelerometerConfig;
            case GYROSCOPE:
                return config instanceof NccGyroscopeConfig;
            case GYROSCOPE_UNCALIBRATED:
                return config instanceof NccGyroscopeUncalibratedConfig;
            case MAGNETIC_FIELD:
                return config instanceof NccMagneticFieldConfig;
            case MAGNETIC_FIELD_UNCALIBRATED:
                return config instanceof NccMagneticFieldUncalibratedConfig;
            case ROTATION_VECTOR:
                return config instanceof NccRotationVectorConfig;
            case BAROMETER:
                return config instanceof NccBarometerConfig;

            // Other
            case BATTERY:
                return config instanceof NccBatteryConfig;
            case LOCATION_MODE:
                return config instanceof NccLocationModeConfig;
            case POWER:
                return config instanceof NccPowerConfig;
            case BLUETOOTH:
                return config instanceof NccBluetoothConfig;
            case WIFI:
                return config instanceof NccWifiConfig;
            case PHONE_CALLS:
                return config instanceof NccPhoneCallsConfig;
            default:
                throw new NccException(TAG, "Unknown sensor type '" + sensorType.getName() + "'", NccExceptionErrorCode.UNKNOWN_ERROR);
        }
    }

    private NccAbstractSensor constructSensor(NccSensorType sensorType, NccConfig config) throws NccException {
        switch (sensorType) {
            // Google Play Services sensors
            case LOCATION:
                return new NccLocation(applicationContext, config);
            case MOTION_ACTIVITY:
                return new NccMotion(applicationContext, config);

            // Native sensors
            case ACCELEROMETER:
                return new NccAccelerometer(applicationContext, config);
            case LINEAR_ACCELEROMETER:
                return new NccLinearAccelerometer(applicationContext, config);
            case GYROSCOPE:
                return new NccGyroscope(applicationContext, config);
            case GYROSCOPE_UNCALIBRATED:
                return new NccGyroscopeUncalibrated(applicationContext, config);
            case MAGNETIC_FIELD:
                return new NccMagneticField(applicationContext, config);
            case MAGNETIC_FIELD_UNCALIBRATED:
                return new NccMagneticFieldUncalibrated(applicationContext, config);
            case ROTATION_VECTOR:
                return new NccRotationVector(applicationContext, config);
            case BAROMETER:
                return new NccBarometer(applicationContext, config);

            // Other
            case BATTERY:
                return new NccBattery(applicationContext, config);
            case LOCATION_MODE:
                return new NccLocationMode(applicationContext, config);
            case POWER:
                return new NccPower(applicationContext, config);
            case BLUETOOTH:
                return new NccBluetooth(applicationContext, config);
            case WIFI:
                return new NccWifi(applicationContext, config);
            case PHONE_CALLS:
                return new NccPhoneCalls(applicationContext, config);
            default:
                throw new NccException(TAG, "Unknown sensor type '" + sensorType.getName() + "'", NccExceptionErrorCode.UNKNOWN_ERROR);
        }
    }

    private NccAbstractSensor getSensor(NccSensorType sensorType) {
        return sensors.get(sensorType.ordinal());
    }

    public boolean isSensorAvailable(NccSensorType sensorType) throws NccException {
        PackageManager packageManager = applicationContext.getPackageManager();

        switch (sensorType) {
            // Google Play Services sensors
            case LOCATION:
                return packageManager.hasSystemFeature(PackageManager.FEATURE_LOCATION);
            case MOTION_ACTIVITY:
                return isGooglePlayServicesAvailable(applicationContext);

            // Native sensors
            case ACCELEROMETER:
            case LINEAR_ACCELEROMETER:
                return packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_ACCELEROMETER);
            case GYROSCOPE:
                return packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_GYROSCOPE);
            case GYROSCOPE_UNCALIBRATED:
                return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2
                    && packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_GYROSCOPE);
            case MAGNETIC_FIELD:
                return packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_COMPASS);
            case MAGNETIC_FIELD_UNCALIBRATED:
                return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2
                        && packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_COMPASS);
            case ROTATION_VECTOR:
                return packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_GYROSCOPE);
            case BAROMETER:
                return packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_BAROMETER);

            // Other sensors
            case BATTERY:
            case LOCATION_MODE:
            case POWER:
            case BLUETOOTH:
            case WIFI:
            case PHONE_CALLS:
                return true;
            default:
                throw new NccException(TAG, "Unknown sensor type '" + sensorType.getName() + "'", NccExceptionErrorCode.UNKNOWN_ERROR);
        }
    }
}
