package com.agero.nccsdk.domain.sensor;

import com.agero.nccsdk.NccException;
import com.agero.nccsdk.NccSensorListener;
import com.agero.nccsdk.domain.config.NccConfig;

/**
 * Created by james hermida on 8/16/17.
 */

interface NccSensor {

    /**
     * Starts the sensor on the device. This is where native calls are made to start the sensor.
     */
    void startStreaming();

    /**
     * Stops the sensor on the device. This is where native calls are made to stop the sensor.
     */
    void stopStreaming();

    /**
     * Gets the streaming state of the sensor
     *
     * @return true if the sensor is streaming, otherwise false
     */
    boolean isStreaming();

    /**
     * Subscribes a listener to the sensor's data
     *
     * @param listener An NccSensorListener to be invoked on new sensor data
     */
    void subscribe(NccSensorListener listener) throws NccException;

    /**
     * Unsubscribes a listener to the sensor's data
     *
     * @param listener The NccSensorListener used when subscribing to the sensor
     */
    void unsubscribe(NccSensorListener listener);

    /**
     * Gets whether the sensor has subscribers or not
     *
     * @return true if the sensor has subscribers, otherwise false
     */
    boolean hasSubscribers();

    /**
     * Sets the configuration for the sensor
     *
     * @param config The configuration to set
     */
    void setConfig(NccConfig config);

    /**
     * Gets the configuration for the sensor
     *
     * @return The configuration for the sensor
     */
    NccConfig getConfig();

    /**
     * Invoked when the config for a sensor has been changed
     *
     * This method should not be called directly.
     *
     * @param config
     */
    void onConfigChanged(NccConfig config);
}
