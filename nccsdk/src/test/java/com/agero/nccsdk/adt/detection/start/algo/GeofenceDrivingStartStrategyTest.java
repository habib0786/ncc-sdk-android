package com.agero.nccsdk.adt.detection.start.algo;

import android.content.Context;
import android.location.Location;

import com.agero.nccsdk.adt.AdtTriggerType;
import com.agero.nccsdk.adt.detection.domain.geofence.GeofenceManager;
import com.agero.nccsdk.adt.detection.domain.geofence.model.NccGeofence;
import com.agero.nccsdk.domain.data.NccGeofenceEventData;
import com.google.android.gms.location.Geofence;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

/**
 * Created by james hermida on 11/30/17.
 */

@RunWith(PowerMockRunner.class)
public class GeofenceDrivingStartStrategyTest {

    private GeofenceDrivingStartStrategy geofenceCollectionStartStrategy;

    @Mock
    private Context mockContext;

    @Mock
    private GeofenceManager mockGeofenceManager;

    @Before
    public void setUp() throws Exception {
        geofenceCollectionStartStrategy = PowerMockito.spy(new GeofenceDrivingStartStrategy(mockContext, mockGeofenceManager));
    }

    @Test
    public void evaluate_null_data() throws Exception {
        geofenceCollectionStartStrategy.evaluate(null);

        verifyZeroInteractions(mockGeofenceManager);
        verify(geofenceCollectionStartStrategy, times(0)).sendCollectionStartBroadcast(any(AdtTriggerType.class));
    }

    @Test
    public void evaluate_invalid_transition() throws Exception {
        NccGeofenceEventData data = new NccGeofenceEventData(0, null, null, Geofence.GEOFENCE_TRANSITION_DWELL, false, 0);

        geofenceCollectionStartStrategy.evaluate(data);

        verifyZeroInteractions(mockGeofenceManager);
        verify(geofenceCollectionStartStrategy, times(0)).sendCollectionStartBroadcast(any(AdtTriggerType.class));
    }

    @Test
    public void evaluate_null_trigger_location() throws Exception {
        NccGeofenceEventData data = new NccGeofenceEventData(0, null, null, Geofence.GEOFENCE_TRANSITION_EXIT, false, 0);

        geofenceCollectionStartStrategy.evaluate(data);

        verify(geofenceCollectionStartStrategy, times(0)).sendCollectionStartBroadcast(any(AdtTriggerType.class));
    }

    @Test
    public void evaluate_null_geofence() throws Exception {
        NccGeofenceEventData data = new NccGeofenceEventData(0, null, new Location(""), Geofence.GEOFENCE_TRANSITION_EXIT, false, 0);

        PowerMockito.when(mockGeofenceManager.getGeofence()).thenReturn(null);

        geofenceCollectionStartStrategy.evaluate(data);

        verify(geofenceCollectionStartStrategy, times(0)).sendCollectionStartBroadcast(any(AdtTriggerType.class));
    }

    @Test
    @PrepareForTest({GeofenceDrivingStartStrategy.class})
    public void evaluate_false_positive() throws Exception {
        float testGeofenceRadius = 500;
        double distanceSmallerThanRadius = testGeofenceRadius - 100;

        Location testTriggerLocation = new Location("");
        testTriggerLocation.setLatitude(0);
        testTriggerLocation.setLongitude(0);

        NccGeofenceEventData data = new NccGeofenceEventData(0, null, testTriggerLocation, Geofence.GEOFENCE_TRANSITION_EXIT, false, 0);
        NccGeofence testGeofence = new NccGeofence("", 0, 0, testGeofenceRadius, 0, 0);

        PowerMockito.when(geofenceCollectionStartStrategy, "calculateDistanceBetween", anyDouble(), anyDouble(), anyDouble(), anyDouble()).thenReturn(distanceSmallerThanRadius);
        PowerMockito.when(mockGeofenceManager.getGeofence()).thenReturn(testGeofence);

        geofenceCollectionStartStrategy.evaluate(data);

        verify(geofenceCollectionStartStrategy, times(0)).sendCollectionStartBroadcast(any(AdtTriggerType.class));
    }

    @Test
    @PrepareForTest({GeofenceDrivingStartStrategy.class})
    public void evaluate_starts_collection_true_positive() throws Exception {
        float testGeofenceRadius = 500;
        double distanceGreaterThanRadius = testGeofenceRadius + 100;

        Location testTriggerLocation = new Location("");
        testTriggerLocation.setLatitude(0);
        testTriggerLocation.setLongitude(0);

        NccGeofenceEventData data = new NccGeofenceEventData(0, null, testTriggerLocation, Geofence.GEOFENCE_TRANSITION_EXIT, false, 0);
        NccGeofence testGeofence = new NccGeofence("", 0, 0, testGeofenceRadius, 0, 0);

        PowerMockito.when(geofenceCollectionStartStrategy, "calculateDistanceBetween", anyDouble(), anyDouble(), anyDouble(), anyDouble()).thenReturn(distanceGreaterThanRadius);
        PowerMockito.doNothing().when(geofenceCollectionStartStrategy, "sendCollectionStartBroadcast", any(AdtTriggerType.class));
        PowerMockito.when(mockGeofenceManager.getGeofence()).thenReturn(testGeofence);

        geofenceCollectionStartStrategy.evaluate(data);

        verify(geofenceCollectionStartStrategy, times(1)).sendCollectionStartBroadcast(AdtTriggerType.START_GEOFENCE);
    }
}
