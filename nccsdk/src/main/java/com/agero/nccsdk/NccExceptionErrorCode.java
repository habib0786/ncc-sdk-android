package com.agero.nccsdk;

/**
 * Defines error codes for the exceptions thrown by the SDK.
 */

public enum NccExceptionErrorCode {
    INITIALIZE_ERROR,
    UNKNOWN_ERROR,
    SENSOR_UNAVAILABLE,
    INVALID_PARAMETER
}
